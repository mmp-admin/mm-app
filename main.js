const electron = require('electron');
const path = require('path');
const url = require('url');

const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

var window = null; // global reference to window object

function createWindow() {
    window = new BrowserWindow({ width: 800, height: 600 });
    window.setFullScreen(true);

    window.loadURL(url.format({
        pathname: path.join(__dirname + '/ui', 'index.html'),
        protocol: 'file:',
        slashes: true
    }));

    window.on('closed', () => {
        window = null;
  });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    app.quit();
})

app.on('activate', () => {
    if (window == null) {
        createWindow();
    }
});