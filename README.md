# README German / English # 

# German #
# Das Projekt #
- Electron APP für den Magic Mirror der Goethe-Schule Flensburg
- Als CSS-Framework wird [MaterializeCSS](http://materializecss.com) verwendet
- Als Basis einen Raspberry PI 2B mit Raspbian

# Funktionen / Ziele #
# MM-APP #
- Vertretungs- und Stundenplan anzeigen [freie Klassenwahl]
- Wetter & Termine [Feiertage, Ferien, ...]
- Minispiele [wie Pong]
- RSS-Feed der Goethe-Schule Flensburg
- RFID Admin Authentifikation
- Goethe-News

# Server #
- Zugriff nur über das lokale Netzwerk [LAN] über SSH und sFTP
- Unverschlüsselte Verbindungen sind jederzeit zu unterlassen
- Cronjobs um 2 bis 3 Uhr morgens für Updates und Scans [APT & ClamTK]
- Iptables als Firewall
- SSH-Port wird geändert und auf den eigenen Laptops werden SSH-Keys mit einer Mindestlänge von 8192bit erstellt
- Passwort wird alle 2 Monate geändert

# Zeitplan #
- Version 1.0.0 sollte zum 21.07.2017 auf den Raspberry PI aufgespielt werden

# English #
# The project #
- Electron APP for the Magic Mirror of Goethe-Schule Flensburg
- We´ll use MaterializeCSS as CSS-Framework using [MaterializeCSS](http://materializecss.com)
- Based on a Raspberry PI 2B with Raspbian
# Function / Goals #
# MM-APP #
- Show substitude teacher plan and schedule [Free class selection]
- Weather & dates [Holidays, ...]
- Minigames [Like Pong]
- RSS-Feed of Goethe-Schule Flensburg
- RFID admin authentification
- Goethe-News

# Server #
- Access only per local network[LAN], per SSH and sFTP
- Unencrypted connections are disallowed and disabled
- Cronjobs at 2 till 3 O´clock pm for updates and scans [APT & ClamTK]
- Iptables as firewall
- SSH-port will be changed and SSH-keys with a minimum length of 8192bit will be created on the own laptops
- Password will be changed every 2 month

# Deadlines #
- Version 1.0.0 should be uploaded on the Rasberry PI till the 21.07.2017

# Credits #
- Leonard Scheuer
- Lennart Heinbach
- Jannik Kudla
- Nils Drewniok
- Johannes Kuta
- Vincent Basurco
- Anton Bischof
- Andreas Matz
- Matthis Radke

(C) 2017