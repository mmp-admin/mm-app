var http = require('http');
var htmlparser = require('htmlparser2');
var feed_content = require('./feed_content.js');

var options = {
  hostname: 'www.goethe.flensburg.de',
  port: 80,
  path: '/rss/feed.xml',
  method: 'GET'
};


var req = http.request(options, (res) => {
  console.log(`STATUS: ${res.statusCode}`);
  console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
  res.setEncoding('utf8');
  var content = '';
  res.on('data', (chunk) => {
    content = content + chunk; 
  });
  res.on('end', () => {
    parse_rss(content);  
  });
});

req.on('error', (e) => {
  console.error(`problem with request: ${e.message}`);
});

function parse_rss(content){
    
    var items = [];
    var current_item = {};
    var current_tag = '';
    var item_status = false;
    var parser = new htmlparser.Parser({
        
        onopentag: function(name, attribs){
            if(name == 'item'){
                item_status = true;
                current_item = {};
            }
            if(item_status){
                current_tag = name;
            }
        },
        
        ontext: function(text){
            if(item_status){
                if(!(current_tag in current_item)){
                    current_item[current_tag] = text;
                }
            }
        },
        
        onclosetag: function(tagname){
            if(tagname == 'item'){
                item_status = false;
                items.push(current_item);
                feed_content(current_item);
            }
        },
        
        onend: function(){
        }
            
    }, {decodeEntities: true});
    parser.write(content);
    parser.end();    
} 

req.end();