var http = require('http');
var htmlparser = require('htmlparser2');

module.exports = function(item){
    var options = {
        hostname: 'www.goethe.flensburg.de',
        port: 80,
        path: '/' + item.guid,
        method: 'GET'
    };
    
    var req = http.request(options, (res) => {
        res.setEncoding('utf8');
        var content = '';
        res.on('data', (chunk) => {
            content = content + chunk; 
        });
        res.on('end', () => {
            parse_rss(content);  
        });
    });
    req.end();
    
    function parse_rss(content){

        var text = '';
        var content_status = false;
        var parser = new htmlparser.Parser({

            onopentag: function(name, attribs){
                if(name == 'div' && attribs.itemprop == 'articleBody'){
                    content_status = true;
                }
            },

            ontext: function(chunk){
                if(content_status){
                    text = text + chunk;
                }
            },

            onclosetag: function(tagname){
                if(tagname == 'div' && content_status){
                    content_status = false;
                    console.log(text);
                }
            },

            onend: function(){
                
            }

        }, {decodeEntities: true});
        parser.write(content);
        parser.end();    
    } 
};